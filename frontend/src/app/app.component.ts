import { ChangeDetectionStrategy, Component, Signal, signal } from '@angular/core';
import { RouterOutlet } from '@angular/router';

@Component({
    selector: 'sda-root',
    standalone: true,
    imports: [RouterOutlet],
    templateUrl: './app.component.html',
    styleUrl: './app.component.scss',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppComponent {
    protected title$: Signal<string> = signal('Heelo');
}
