import { BrowserWindow, app } from 'electron';
import path from 'path';
import url from 'url';

let mainWindow: BrowserWindow | null;

/**
 * App initialization
 */
function createWindow(): void {
    mainWindow = new BrowserWindow({
        width: 800,
        height: 600,
        webPreferences: {
            nodeIntegration: true,
            contextIsolation: false,
            preload: path.join(__dirname, './preload.js'),
        },
    });

    mainWindow.loadURL(
        url.format({
            pathname: path.join(__dirname, '../..', 'frontend/dist/browser/index.html'), 
            protocol: 'file:',
            slashes: true,
        })
    );

    mainWindow.on('closed', function () {
        mainWindow = null;
    });
}

app.whenReady().then(() => {
    createWindow();
  
    app.on('activate', function () {
        if (BrowserWindow.getAllWindows().length === 0) createWindow();
    });
});
  
app.on('window-all-closed', () => {
    if (process.platform !== 'darwin') {
        app.quit();
    }
});