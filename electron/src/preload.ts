window.addEventListener('DOMContentLoaded', () => {
    const replaceText = (selector: string, text: string): void => {
        const element: HTMLElement | null = document.getElementById(selector);
        if (element) {
            element.innerText = text;
        }
    };
  
    for (const type of ['chrome', 'node', 'electron']) {
        replaceText(`${type}-version`, process.versions[type as keyof NodeJS.ProcessVersions] as string);
    }
});